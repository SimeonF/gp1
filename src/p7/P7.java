package p7;

public class P7 {
	public static void main(String[] args) {
		Punkt p1 = new Punkt(5,8);
		Punkt p2 = new Punkt(4,7);
		
		p1.anzeigen();
		p2.anzeigen();
		
		System.out.println(Punkt.punktAbstand(p1, p2));
		System.out.println(p1.abstandVon(p2));
	}

}

package p7;

public class Punkt {
	double x, y;
	Punkt(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	static double punktAbstand(Punkt p1, Punkt p2) {
		return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
	}

/*	static double punktAbstand(Punkt p1, Punkt p2) {
		return p1.abstandVon(p2);
	}
*/	
	double abstandVon(Punkt p2) {
		return Math.sqrt((this.x-p2.x)*(this.x-p2.x)+(this.y-p2.y)*(this.y-p2.y));
	}
	
	void anzeigen() {
		System.out.println("("+x+","+y+")");
	}
}

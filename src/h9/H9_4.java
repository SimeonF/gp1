package h9;

public class H9_4 {
	static int k = Integer.MAX_VALUE;
	public static void main(String[] args) {
		k = Integer.parseInt(args[1]);
		int n = Integer.parseInt(args[0]);
		try{ hanoi(n, 'A', 'B', 'C'); }
		catch(Exception e) { System.out.println(e); }
	}

	static void printhanoi(int hoehe, char quelle, char ziel) throws Exception{
		System.out.println ("Setze Scheibe "+hoehe+" von "+quelle+" nach "+ziel);
		k--;
		if(k<1) throw new Exception("hier abgebrochen");
	}

	static void hanoi (int hoehe, char quelle, char ablage, char ziel) throws Exception{
		if (hoehe == 1) printhanoi(hoehe, quelle, ziel);
		else {
			hanoi (hoehe - 1, quelle, ziel, ablage);
			printhanoi(hoehe, quelle, ziel);
			hanoi (hoehe - 1, ablage, quelle, ziel);
		}
	}
}
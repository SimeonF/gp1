package h9;

import java.io.*;
import java.util.Scanner;

public class H9_1 {
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		File file = null; 
		do {
			System.out.println("Welche Datei soll eingegeben werden?");
			file = new File(input.nextLine());
		} while (!file.exists());
		Scanner haystack = new Scanner(file);
		System.out.println("Wonach soll gesucht gesucht werden?");
		String needle = input.nextLine();
		System.out.println("Es wird nach '"+needle+"' in "+file.getName()+" gesucht.");

		PrintStream haystream = new PrintStream(new File("hay.out"));
		PrintStream needlestream = new PrintStream(new File(needle+".out"));
		while (haystack.hasNextLine()) {
			String line = haystack.nextLine();
			if (line.contains(needle)) needlestream.println(line);
			else haystream.println(line);
		}
		input.close();
		haystack.close();
		haystream.close();
		needlestream.close();
	}
}
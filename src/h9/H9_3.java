package h9;

public class H9_3 {
	public static void main (String[] args) {
		int size = Integer.parseInt (args[0]);
		for (int i = 0; i<size; i++) System.out.print ("Var"+(i + 1)+"\t");
		System.out.print("\n\n"+printIt ("", size));
	}

	static String printIt (String ret, int size) {
		if (size == 0) return ret + "\n";
		return printIt (ret+"false\t", size - 1) + printIt (ret+"true\t", size - 1);
	}
}
package h9;

import java.util.Scanner;

public class H9_2_1 {
	public static void main(String[] args) {
		Scanner word = new Scanner(args[0]);
		while(word.hasNext()) System.out.println(word.next());
		word.close();
	}
}
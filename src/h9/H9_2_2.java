package h9;

import java.util.*;

public class H9_2_2 {
	public static void main(String[] args) {
		Scanner word = new Scanner(args[0]);
		HashMap<String,Integer> wordlist = new HashMap<String,Integer>();
		while(word.hasNext()) {
			String actualword = word.next();
			int actualcount = 0;
			
			if(wordlist.containsKey(actualword)) actualcount = wordlist.get(actualword);
			actualcount++;
			wordlist.put(actualword, actualcount);
		}
		System.out.println(wordlist);
		word.close();
	}
}
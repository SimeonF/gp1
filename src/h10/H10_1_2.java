package h10;

public class H10_1_2 {
	public static void main(String[] args) {
		Liste abzaehlvers = new Liste(1);
		for (int i=2;i<=Integer.parseInt(args[0]);i++)
			abzaehlvers.lappend(new Liste(i));

		Liste temp = abzaehlvers;
		while (temp.link != null)
			temp = temp.link;	
		temp.link = abzaehlvers;
		
		for (int i=0;abzaehlvers.link!=abzaehlvers;i++) {
			if (i%Integer.parseInt(args[1])==1) {
				Liste out = abzaehlvers.schließeNachfolgerAus();
				System.out.println(out.data);
			}
			else abzaehlvers = abzaehlvers.link;
		}
		System.out.println(abzaehlvers.data);
	}
}

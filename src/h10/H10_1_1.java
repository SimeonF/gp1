package h10;

public class H10_1_1 {
	public static void main(String[] args) {
		int n = 10-Integer.parseInt(args[0]);
		Liste list = new Liste(1);
		for (int i=2;i<=n;i++)
			list.lappend(new Liste(i));
		
		Liste temp = list;
		while (temp.link != null)
			temp = temp.link;	
		temp.link = list;
		
		System.out.println(out(list,10));
	}
	
	public static String out(Liste list, int toPrint) {
		if (toPrint == 1)
			return list.data+" ";
		else
			return list.data+" "+out(list.link, toPrint-1);
	}
}
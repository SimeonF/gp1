package p8;

public class P8_2 {
	public static void main(String[] args) {
		Punkt p1 = new Punkt(1,6);
		Punkt p2 = new Punkt(4,6);
		Punkt p3 = new Punkt(7,10);
		
		Dreieck d1 = new Dreieck(p1, p2, p3);
		d1.anzeigen();
		System.out.println(d1.umfang());
		
		d1.verschiebeUm(4, 7);
		d1.anzeigen();
		System.out.println(d1.umfang());
		
	}
}

package h4;

public class H4_1_2 {
	public static void main(String[] args) {
		int n = 39;
		int k = 6;
		
		if (n<k) {
			System.out.println("N muss groesser sein, als K!");
		} else {
			double noverk = faculty(n) / (faculty(k) * faculty(n-k));
			System.out.println(n + " ueber " + k + " ist gleich " + noverk);
		}
	}

	static double faculty (int n) {
		double ret = 1;
		while (n > 0) {
			ret *= n;
			n--;
		}
		return ret;
	}
}

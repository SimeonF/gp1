package h4;

public class H4_3 {
	public static void main(String[] args) {
		int zahl = Integer.parseInt(args[0]);
		int stellen = Integer.parseInt(args[1]);
		System.out.println(formatierteAusgabe(zahl, stellen));
	}

	static int stellen(int zahl) {
		int stellen = 0;
		while (zahl/(int)Math.pow(10.0, (double) stellen ) != 0) {
			stellen++;
		}
		return stellen;
	}
	
	static int ziffer(int zahl,int nummer) {
		zahl = Math.abs(zahl);
		while (nummer-- > 1) {
			zahl /= 10;
		}
		return zahl % 10;
	}
	
	static String formatierteAusgabe(int zahl, int stellen) {
		String out = "";
		if (stellen(zahl) > stellen || (zahl < 0 && stellen(zahl)+1 > stellen)) {
			for (int i=1; i <= stellen; i++) {
				out += "#";
			}
			return out;
		} else {
			int rest = stellen;
			if (stellen(zahl) == 0) {
				out = "0";
				rest--;
			} else {
				for (int i = 1; i <= stellen(zahl); i++) {
					out = ziffer(zahl,i) + out;
					rest--;
				}
			}
			if (zahl < 0) {
				out = "-" + out;
				rest--;
			}
			for (int i = 1; i <= rest; i++) {
				out = "." + out;
			}
			return out;
		}
	}
}

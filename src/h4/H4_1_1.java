package h4;

public class H4_1_1 {
	public static void main(String[] args) {
		for (int i=1; i< 20; i++) {
			System.out.println(i + "! = " + faculty(i));
		}
	}

	static int faculty (int n) {
		int ret = 1;
		while (n > 0) {
			ret *= n;
			n--;
		}
		return ret;
	}
}

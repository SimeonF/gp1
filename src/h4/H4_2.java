package h4;

public class H4_2 {
	public static void main(String[] args) {
		double ug = Double.parseDouble(args[0]);
		double og = Double.parseDouble(args[1]);
		
		if (ug > og) {
			double help = ug;
			ug = og;
			og = help;
			System.out.println("Untere und obere Grenze getauscht!");
		}
		//System.out.println((int) (og / Math.PI) * Math.PI);		// gibt eigentlich schon die gesuchte Nullstelle zurueck
		System.out.println(nullstelle(ug,og));
	}

	static double nullstelle (double ug, double og) {
		double neuegrenze = og-(og-ug)/2;
		while (ug < og - 0.00001) {
			neuegrenze = og-(og-ug)/2;
			if (Math.sin(ug)*Math.sin(neuegrenze) <= 0) {
				og = neuegrenze;
			}
			else {
				ug = neuegrenze;
			}
		}
		return neuegrenze;
	}
}

package p6;

public class P6_2_1 {
	public static void main(String[] args) {
//		int base = Integer.parseInt(args[0]);
//		int exponent = Integer.parseInt(args[1]);
		int base = 20;
		int exponent = 0;
		
		System.out.println(exp(base,exponent));
	}
	
	static int exp(int base, int exponent) {
		if (exponent < 0) System.out.println("Der Exponent darf nicht negativ sein!!!");
		if (exponent < 1) return 1;
		int ret = base * exp(base,exponent-1);
		return ret;
	}
}

package p6;

public class P6_2_2 {
	public static void main(String[] args) {
//		int base = Integer.parseInt(args[0]);
//		int exponent = Integer.parseInt(args[1]);
		int base = 20;
		int exponent = -1;
		
		System.out.println(exp(base,exponent));
	}
	
	static int exp(int base, int exponent) {
		if (exponent < 0){
			return nexp(base,exponent);
		}
		if (exponent == 0) return 1;
		int ret = base * exp(base,exponent-1);
		return ret;
	}
	
	static int nexp(int base, int exponent) {
		if (base == 0) return 0;
		if (exponent == -1) return 1/base;
		int ret = 1/(base * nexp(base,exponent+1));
		return ret;
	}
}

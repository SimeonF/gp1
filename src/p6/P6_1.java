package p6;

public class P6_1 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		System.out.println(faculty(n));
	}
	
	static int faculty(int n){
		System.out.println("Rufe auf: faculty("+n+")");
		int ret = 1;
		if (n > 1) {
			ret = n * faculty(n-1);
			System.out.println("Habe gerade eben "+n+" mal "+(n-1)+"! gerechnet.");
		}
		System.out.println("Das Ergebnis von "+n+"! ist "+ret); 
		return ret;
	}
}

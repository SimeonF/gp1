package p2;
public class P2_3 {
	public static void main(String[] args) {
		for (int y = 1; y <= 10; y++) { //y nach oben, x nach rechts
			for (int x = 1; x <= 10; x++) {
				System.out.print(x*y + "\t");
			}
			System.out.print("\n");
		}
		System.out.println("\n\n\n\n\n\n");
		int a = 1;
		while (a <= 10) {
			int b = 1;
			while (b <= 10) {
				System.out.print(a*b + "\t");
				b++;
			}
			System.out.print("\n");
			a++;
		}
	}
}

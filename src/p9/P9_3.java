package p9;

import java.io.*;
import java.util.Scanner;

public class P9_3 {
	public static void main(String[] args) throws IOException {
		Scanner file = new Scanner(new File("eingabe.in"));
			
		PrintStream andere = new PrintStream(new File("andere.out"));
		PrintStream er = new PrintStream(new File("er.out"));
		while (file.hasNextLine()) {
			String line = file.nextLine();
			if (line.contains("er")) {
				er.println(line);
			} else andere.println(line);
		}
		er.close();
		andere.close();
		file.close();
	}
}

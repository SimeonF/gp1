package h8;

public class H8_1 {
	public static void main(String[] args) {

		Punkt p1 = new Punkt(6,10);
		Punkt p2 = new Punkt(p1);
		p2.verschiebeUm(3, 7);
		System.out.println("P1: "+p1+"\nP2: "+p2);
		
		Punkt p3 = new Punkt(42,1337);
		
		Dreieck d1 = new Dreieck(p1,p2,p3);
		Dreieck d2 = new Dreieck(d1);
		d2.verschiebeUm(5, 7);
		System.out.println("\tD1: \n"+d1+"\n\tD2: \n"+d2);
		
	}
}

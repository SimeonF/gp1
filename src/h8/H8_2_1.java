package h8;

public class H8_2_1 {
	public static void main(String[] args) {
		int[][] matrix1 = {
				{5,6,7},
				{4,8,9}
		};
		int[][] matrix2 = {
				{7,4,1,2},
				{8,3,5,6},
				{9,5,1,3}
		};
		
		int[][] produktmatrix = new int [matrix1.length][matrix2[0].length];
		
		for(int c=0;c<produktmatrix.length;c++) {
			for(int l=0;l<produktmatrix[c].length;l++) {
				for(int i=0; i<matrix1[c].length; i++) {
					produktmatrix[c][l] += matrix1[c][i]*matrix2[i][l];
				}
				System.out.print(produktmatrix[c][l]+"\t");
			}
			System.out.println();
		}
		
	}
}

package h8;

public class Dreieck {
	Punkt p1, p2, p3;
	
	public Dreieck(Dreieck d1) {
		this.p1 = new Punkt(d1.p1);
		this.p2 = new Punkt(d1.p2);
		this.p3 = new Punkt(d1.p3);
	}
	
	public Dreieck(Punkt p1, Punkt p2, Punkt p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	
	public String toString() {
		return "Punkt 1: "+p1+"\nPunkt 2: "+p2+"\nPunkt 3: "+p3;
	}
	public void anzeigen() {
		System.out.println(this);
	}
	
	public void verschiebeUm(double dx, double dy) {
		p1.verschiebeUm(dx, dy);
		p2.verschiebeUm(dx, dy);
		p3.verschiebeUm(dx, dy);
	}
	public double umfang() {
		return Punkt.abstand(p1, p2)+Punkt.abstand(p2, p3)+Punkt.abstand(p3, p1);
	}
}

package h8;

public class Punkt {
	double x, y;
	
	public Punkt(Punkt p1) {
		this.x = p1.x;
		this.y = p1.y;
	}
	public Punkt(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static Punkt liesPunkt(String[] pts, int i) {
		return new Punkt(Double.parseDouble(pts[2*i-2]), Double.parseDouble(pts[2*i-1]));
	}

	public static double abstand(Punkt p1, Punkt p2) {
		return p1.abstand(p2);
	}

	public String toString() {
		return"("+x+","+y+")";
	}
	public void anzeigen() {
		System.out.println(this);
	}
	public void verschiebeUm(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}
	public double abstand(Punkt p2) {
		return Math.sqrt((this.x-p2.x)*(this.x-p2.x)+(this.y-p2.y)*(this.y-p2.y));
	}
}
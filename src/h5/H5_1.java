package h5;

public class H5_1 {
	public static void main(String[] args) {
		boolean nothing = false;
		boolean constant = true;
		boolean increase = false;
		boolean decrease = false;
		
		for (int i=0;i < args.length-1;i++) {
			double num1 = Double.parseDouble(args[i]);
			double num2 = Double.parseDouble(args[i+1]);

			if (num1 < num2) {
				constant = false;
				increase = true;
				if (decrease) nothing = true;
			} else if (num1 > num2) {
				constant = false;
				decrease = true;
				if (increase) nothing = true;
			}
			if (nothing) i = args.length;
		}

		if (args.length == 1) System.out.println("EINE Zahl ist KEINE Folge!");

		else if (nothing) System.out.println("Die Folge ist nicht monoton.");
		else if (constant) System.out.println("Die Folge ist konstant.") ;
		else if (increase) System.out.println("Die Folge ist monoton ansteigend.");
		else if (decrease) System.out.println("Die Folge ist monoton absteigend.");
		else System.out.println("Unbekannter Fehler :(");
	}
}

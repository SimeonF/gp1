package h5;

public class H5_3 {
	public static void main( String[] args ) { // hier startet das Programm
		final int size = 5 ;
		int matrix[][] = new int [size][size] ;
		int zeile, spalte ;
	// Teil 1: Matrix auffu??llen - sollen Sie unver??ndert lassen !!
		for ( zeile = 0 ; zeile < size ; zeile++ ) {
			for ( spalte = 0 ; spalte < size ; spalte++ ) {
				matrix[ zeile ][ spalte ] = (spalte+1) * 10 + (zeile+1) ; // So entspricht das der Beispiel-Ausgabe!!
			}
		}
		mprint(size,matrix);

	// Teil 2: Matrix spiegeln ??? hier sollen Sie etwas programmieren
		for ( zeile = 0 ; zeile < size ; zeile++ ) {
			int newsize = size-zeile-1;
			for ( spalte = 0 ; spalte < newsize ; spalte++ ) {
				int neuezeile = size-1-spalte;
				int neuespalte = size-1-zeile;

				int hilf = matrix[zeile][spalte];
				matrix[zeile][spalte] = matrix[neuezeile][neuespalte];
				matrix[neuezeile][neuespalte] = hilf;
			}
		}
		System.out.println();
		mprint(size,matrix);
	}

	static void mprint (int size, int matrix[][]) {
		// Teil 3: Matrix ausgeben - sollen Sie unver??ndert lassen !
		int zeile,spalte;
		for ( zeile = 0 ; zeile < size ; zeile ++ ) {
			for ( spalte = 0 ; spalte < size ; spalte ++ ) {
				System.out.print( " " + matrix[ zeile ][ spalte ] ) ;
			}
			System.out.println() ;
		}
	}

}
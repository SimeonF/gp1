package p1;
public class P1_3_c {
	public static void main(String[] args) {
		int zahl1 = 1;
		int zahl2 = 10;
		int zahl3 = 100;
		int zahl4 = 1000;
		
		/*{zahl1 = v1 UND zahl2 = v2 UND zahl3 = v3 UND zahl4 = v4}*/
		
		//BEGIN own code
		int help = zahl1;
		/*{zahl1 = v1 UND zahl2 = v2 UND zahl3 = v3 UND zahl4 = v4 UND help = v1}*/
		zahl1 = zahl4;
		/*{zahl1 = v4 UND zahl2 = v2 UND zahl3 = v3 UND zahl4 = v4 UND help = v1}*/
		zahl4 = zahl3;
		/*{zahl1 = v4 UND zahl2 = v2 UND zahl3 = v3 UND zahl4 = v3 UND help = v1}*/
		zahl3 = zahl2;
		/*{zahl1 = v4 UND zahl2 = v2 UND zahl3 = v2 UND zahl4 = v3 UND help = v1}*/
		zahl2 = help;
		/*{zahl1 = v4 UND zahl2 = help = v1 UND zahl3 = v2 UND zahl4 = v3 UND help = v1}*/
		//END own code
		
		/* {zashl1 = v4 UND zahl2 = v1 UND zahl3 = v2 UND zahl4 = v3}*/
		
		System.out.println( "zahl1 hat jetzt den Wert "+ zahl1 );
		System.out.println( "zahl2 hat jetzt den Wert "+ zahl2 );
		System.out.println( "zahl3 hat jetzt den Wert "+ zahl3 );
		System.out.println( "zahl4 hat jetzt den Wert "+ zahl4 );
		}

}

package p4;


public class P4_1
	{
	public static void main( String[] args ) // hier startet das Programm
	{
		double zahl = Double.parseDouble(args[0]) ; // (*)
		System.out.println("Jetzt berechne ich die Kubikzahl von " + zahl) ;
		System.out.println( "Kubik von " + zahl + " ist : " + kubik(zahl)) ;
	} // hier ist die main-Prozedur zuende
	static double kubik(double num) // berechnet Kubik einer gegebenen Zahl
	{
		num = num*num*num;
		return num;
		// hier deklarieren Sie bei Bedarf lokale Variablen
	} // hier ist die Funktion kubik zuende
} // hier ist die Klasse zuende
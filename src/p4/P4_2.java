package p4;


public class P4_2
	{
	public static void main(String[] args) {
		double number1 = Double.parseDouble(args[0]);
		double number2 = Double.parseDouble(args[0]);
		
		System.out.println( "Mittelwert von " + number1 + " und " + number2 + " ist : " + mittelwert(number1,number2));
	}

	static double mittelwert(double med_num1, double med_num2) {
		return (med_num1 + med_num2)/2;
	}
}
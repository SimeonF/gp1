package h7;

public class Bruchzahl {
	private long zaehler, nenner;

	public Bruchzahl(Bruchzahl b1) {
		this.zaehler = b1.zaehler;
		this.nenner = b1.nenner;
	}
	public Bruchzahl(long zaehler, long nenner) {
		this.zaehler = zaehler;
		this.nenner = nenner;
	}

	public static Bruchzahl multipliziere(Bruchzahl b1, Bruchzahl b2) {
		return new Bruchzahl(b1.zaehler * b2.zaehler, b1.nenner * b2.nenner);
	}
	public static Bruchzahl gekuerzt(Bruchzahl b1) {
		Bruchzahl b2 = new Bruchzahl(b1);
		b2.kuerze();
		return b2 ;
	}

	public String toString() {
		return zaehler+"/"+nenner;
	}
	public void zeige() {
		System.out.println(this.toString());
	}
	public void multipliziereMit(long x) {
		this.zaehler *= x;
	}
	public void dividiereDurch(long x) {
		this.nenner *= x;
	}
	public void kuerze() {
		long a = this.zaehler;
		long b = this.nenner;
		while (a != b) {
			if(a > b)	a -= b;
			else		b -= a;
		}
		this.kuerze(a);
	}
	public void kuerze(long x) {
		this.nenner /= x;
		this.zaehler /= x;
	}
}

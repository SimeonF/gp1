package h7;

public class H7_2 {

	public static void main(String[] args) {
		System.out.println("a)");
		Bruchzahl b1 = new Bruchzahl(3,4);
		b1.zeige();
		
			System.out.println("b)");
		Bruchzahl b2 = new Bruchzahl(5,6);
			System.out.println("Neuer Bruch (b2): "+b2.toString());
		b2.multipliziereMit(4);
			System.out.println("b2 multipliziert mit 4: "+b2.toString());
		b2.dividiereDurch(4);
			System.out.println("b2 dividiert durch 4: "+b2.toString());

			System.out.println("c)");
		Bruchzahl b3 = Bruchzahl.multipliziere(b1, b2);
			System.out.println("Neuer Bruch (b3): "+b1+" * "+b2+" = "+b3.toString());
		
			System.out.println("d)");
			System.out.println("W??hrend multipliziereMit() das Objekt ver??ndert, auf dass sie aufgerufen wird,"
			 +"\n erzeugt multipliziere() ein neues Objekt und l??sst die alten unangetastet."
			 +"\n Deshalb wird keine Variable ge??ndert (\"this.zaehler *= x\"),"
			 +"\n sondern ein neues Objekt erzeugt (\"new Bruchzahl(...)\").");
			 

			System.out.println("e)");
		b3.kuerze(3);
			System.out.println("b3 mit 3 gekuerzt: "+b3.toString());
		
			System.out.println("f)");
		Bruchzahl b4 = Bruchzahl.gekuerzt(b3);
			System.out.println("Neuer Bruch (b4): b3 gek??rzt: "+b4.toString());
			System.out.println("B3 ist weiterhin: "+b3.toString());
		
	}
}

package h7;

public class H7_1 {
	public static void main(String[] args) {
		Punkt punkt1 = new Punkt(0,0);
		Punkt punkt2 = new Punkt(0,0);
		double umfang = punkt1.abstand(punkt2);

		System.out.println("Gelesene Eckpunkte:");
		for (int i=1; i<=args.length/2;i ++) {
			// 2. Array w??re sch??ner, aber dann halt mit alternierenden Vars
			if(i%2 == 1) {	punkt1 = Punkt.liesPunkt(args, i); punkt1.anzeigen(); }
			else 		 {	punkt2 = Punkt.liesPunkt(args, i); punkt2.anzeigen(); }
			if(i > 1)		umfang += punkt1.abstand(punkt2);
		}
		if(Integer.parseInt(args[0]) != Integer.parseInt(args[args.length-2]) ||
				Integer.parseInt(args[1]) != Integer.parseInt(args[args.length-1]))
			System.out.println("Kein korrekt geschlossenes Polygon!");
		else {
			System.out.println("Der Polygonumfang ist "+umfang);
		}
	}
}
package h7;

public class H7_3 {
	public static void main(String[] args) {
		char[ ] letters = args[0].toCharArray();

		char lowchar_2 = 'z';
		char lowchar_1 = 'z';
		int pos_2 = 0;
		int pos_1 = 0;

		for (int i=0;i<args[0].length();i++) {
//			System.out.print("1: "+letters[i]+"<="+lowchar_1+"="+(letters[i]<=lowchar_1));
//			System.out.println("\t2: "+letters[i]+"<="+lowchar_2+"="+(letters[i]<=lowchar_2));
			if(letters[i] <= lowchar_1) {
				lowchar_2 = lowchar_1;
				lowchar_1 = letters[i];
				pos_2 = pos_1;
				pos_1 = i+1;
//				System.out.println("lowest: "+lowchar_1+", second: "+lowchar_2);
			} else if(letters[i] <= lowchar_2) {
				lowchar_2 = letters[i];
				pos_2 = i+1;
//				System.out.println("lowest: "+lowchar_1+", second: "+lowchar_2);
			}
		}
		System.out.println("Der zweitkleinste Buchstabe, "+lowchar_2+", ist der "+pos_2+". Buchstabe im Wort.");
	}
}
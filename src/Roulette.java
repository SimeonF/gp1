public class Roulette {
	public static void main(String[] args) {
		int rounds = 100;
		int money = 501;
		int initbet = 1;

		int right = 0;
		int bet = initbet;
		System.out.println("Money: "+money+"\tBet: "+bet);
		for (int i = 1; i <= rounds; i++) {
			if (money < bet*2) break;
			double number = Math.random()*37;
			if (number < 19) {
				System.out.print("Falsche Farbe!\t");
				money -= bet;
				bet = bet * 2;
				rounds++;
			} else {
				System.out.print("Richtige Farbe!\t");
				money += bet;
				bet = initbet;
				right++;
			}
			System.out.println("Money: "+money+"\nBet: "+bet+"\t Money: "+(money-bet));
		}
		System.out.println("Richtige: "+right+" von "+rounds+" ("+right*100/rounds+"%)");
	}
}

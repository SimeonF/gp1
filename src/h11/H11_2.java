package h11;

public class H11_2 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int[] arr = new int[n];

		for (int i=0;i<n;i++)
			arr[i] = i+1;

		printpermut(arr);
	}
	
	public static void printpermut(int[] numbers) {
		printpermut(numbers,"");
	}
	private static void printpermut(int[] numbers, String s) {
		if (numbers.length == 0) System.out.println(s);
		for (int i=0;i<numbers.length;i++) {
			int[] temp = new int[numbers.length-1];
			for (int j=0;j<i;j++) temp[j] = numbers[j];
			for (int j=i+1;j<numbers.length;j++) temp[j-1] = numbers[j];

			printpermut(temp,s+numbers[i]+"\t");
		}
	}
}
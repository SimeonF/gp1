package h11;

public class Tree {
	int value;
	Tree left, right;

	Tree(int i, Tree l, Tree r) {
		value=i;
		left=l;
		right=r;
	}
	Tree (int nodes) {
		this.value=nodes/3;
		this.left = null;
		this.right = null;
		
		for (int i=1; i<=nodes; i++)
			this.insert(i % 7);
	}
	
	public String inOrder() { 
		return (left==null ? "" : left.inOrder())
				+" "+value+" "+
				(right==null ? "" : right.inOrder());
	}
	public void insert(int i) {
		if (i<value)
			if (left==null)
				left = new Tree(i , null , null);
			else left.insert(i);
		else
			if (right==null)
				right = new Tree(i , null , null);
			else right.insert(i);
	}
	
	public int depth() {
		int depth_l = 0;
		int depth_r = 0;
		if (this.left != null)
			depth_l = this.left.depth();
		if (this.right != null)
			depth_r = this.right.depth();
		return 1+Math.max(depth_l, depth_r);
	}
	
	public void printTree() {
		this.printTree("");
	}
	private void printTree(String s) {
		if (this.left != null) this.left.printTree(s+"\t");
		System.out.println(s+this.value);
		if (this.right != null) this.right.printTree(s+"\t");
	}
	
	public void mirrorTree() {
		this.mirrorTree("");
	}
	private void mirrorTree(String s) {
		if (this.right != null) this.right.mirrorTree(s+"\t");
		System.out.println(s+this.value);
		if (this.left != null) this.left.mirrorTree(s+"\t");
	}
	
	public static int counteven(Tree t) {
		int even = 0;
		if (t.left != null) even += counteven(t.left);
		if (t.right != null) even += counteven(t.right);
		if (t.value%2 == 0) even++;
		return even;
	}
	
	
}
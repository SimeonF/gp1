package h11;

public class H11_1_2 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		Tree t = new Tree(n);
		
		t.printTree();
		System.out.println("\n##############################################\n");
		t.mirrorTree();

	}

}

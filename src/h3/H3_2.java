package h3;

public class H3_2 {

	//	Funktionen:
	//		"Spieler"-Zug
	static int playerdraw (int sticks) {
		double draw = Math.random() * 3 + 1;
		sticks -= (int) draw;
		System.out.println("Der Gegenspieler nimmt (zuf??llig) "+(int)draw+", ??brig sind noch "+sticks);
		return sticks;
	}

	//		PC-Zug:
	static int computerdraw (int sticks) {
		int draw = 0;
		for (int i = 1; i <= 3; i++) {
			if ((sticks-i)%4 == 0) {
				draw = i;
				break;
			}
		}
		sticks -= draw;
		System.out.println("Der Computerspieler nimmt "+draw+", ??brig sind noch "+sticks);
		return sticks;
	}

	public static void main (String[]args) {
		int sticks = 53;

		// Erster Zug
		if (sticks%4 != 0) {
			System.out.println("Das Programm m??chte anfangen.\n");
			sticks = computerdraw(sticks);
		} else System.out.println("Das Programm l??sst dem Gegenspieler den Vortritt.\n");

		// Alle weiteren Z??ge
		//{INV} = {sticks % 4 == 0}
		while (sticks > 0) {
			//{INV und sticks >0}
			sticks = playerdraw(sticks);
			// {!INV, denn sticks-(1 bis 3) ist nicht %4 == 0}
			sticks = computerdraw(sticks);
			// {INV}
		}
		//{INV und sticks = 0}
	}
}
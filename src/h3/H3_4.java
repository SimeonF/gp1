package h3;

public class H3_4 {
	public static void main(String [] args)
	{
		int c1 = Integer.parseInt(args[0]) ;
		int c2 = Integer.parseInt(args[1]) ;
		int c3 = Integer.parseInt(args[2]) ;
		int c4 = Integer.parseInt(args[3]) ;

		if (c1 == c2) {
			if (c1==c4) System.out.println(3 * c1 + c3);
			System.out.println(c1 * (c1 + c3));
		}
	}
}

/* Schritte:
	1.  F?r die bessere Ansicht geschweifte Klammern um die beiden Hauptbl?cke gelegt
	2.	(c2-c1==0) und (c1-c2==0) sind das gleiche, wie (c1=c2)
	3.	Die If-Bl?cke zusammengefasst zu 		if ((c1==c2) && ( c1 != c4 )) {		und		if ((c1==c4) && (c1==c2)) {
	4.	(c1==c2) "ausgeklammert"
	5.	(c1!=c4) ist die Negation von (c1==c4), also kann ich statt (c1!=c4) ein else einsetzen
	6.	Da die erste Schleife nur ausgef?hrt wird, wenn c1==c2 ist, kann c1 niemals kleiner sein, als c2, deshalb kann man den If-Block komplett streichen
	7.	In der 2. Schleife bei "c3 = c2 * c3" das 2. c3 durch "c3 = c1 + c3" ersetzt
	8.	In den Ausgaben c2 durch c1 und c3 durch das zuvor gerechnete ersetzt und mathematisch vereinfacht (In die 2. Ausgabe der 1. Schleife wurden beide vorherigen Rechenschritte eingesetzt)
	9.	Die Rechnung, die in beiden If-Bl?cken vorkam eine Ebene nach oben gesetzt
	10.	Den If-Block zu einem Einzeiler gemacht
*/
package h3;

public class H3_1{
	public static void main(String[] args){
		int num = Integer.parseInt(args[0]);
		int max = 0;
		int position = 0;
		int divisor = 1;
		
		while(num/divisor>0){
			if(num%(divisor*10)/divisor > max){
				max = num%(divisor*10)/divisor;
				position = divisor;
			}
			divisor *= 10;
		}
		System.out.println("Maximum: "+max +" Position: " + position + "er");
	}
}
package h3;

public class H3_3 {
	public static void main(String[] args) {
		int zahl = 1024;
		System.out.println("Primfaktorzerlegung\nDie zu zerlegende Zahl lautet: " + zahl);
		teilen(zahl);
	}
	static void teilen (int zahl) {
		int i;
		int max;
		max = (int) Math.round(Math.sqrt((double) zahl));
		if (zahl <= 1) return;
		if (zahl%2 == 0) {
			System.out.print("2 ");
			teilen(zahl/2);
			return;
		}
		for (i=3; i<=max; i=i+2) {
			if (zahl%i == 0) {
				teilen(i);
				teilen(zahl/i);
				return;
			}
		}
	System.out.print(zahl + " ");
	}
}
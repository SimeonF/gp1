package p10;

public class Liste {
	Integer data;
	Liste link;
		
	Liste(Integer data) {
		this.data = data;
		this.link = null;
	}
	
	public String toString() {
		if(link != null) {
			return this.data+"->"+link.toString();
		}return ""+this.data;
	}
		
	public void inc(){
		this.data += 10;
		if(this.link != null) link.inc();
	}
	
	public void lappend(Liste list) {
		if (this.link == null) this.link = list;
		else this.link.lappend(list);
	}
	
	public Liste copy() {
		Liste listcopy = new Liste(this.data);
		if(this.link != null) listcopy.link = link.copy();
		return listcopy;
	}
		
	
		
}

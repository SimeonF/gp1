package h1;
public class H1_1 {
	public static void main(String[] args) {
		int zahl1 = -34;
		/* {zahl1 = -34} */
		int zahl2 = zahl1 + 4;
		/* {zahl2 = -30 UND zahl1 = -34} */
		zahl1 = zahl2 - 10;
		/* {zahl2 = Wurzel(900) = -30 UND zahl1 = Wurzel(1600) = -40 (40 ungeeignet) } */
		zahl1 = zahl1 * zahl1;
		/* {zahl2 = Wurzel(900) = -30 (30 ungeeignet) UND zahl1 = 1600} */
		zahl2 = zahl2 * zahl2;
		/* {zahl2 = 900 UND zahl1 = 1600} */
		System.out.println("zahl1 = " + zahl1 + " und zahl2 = " + zahl2);
	}

}

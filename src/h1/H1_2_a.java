package h1;
public class H1_2_a {
	public static void main(String[] args) {
		int num1 = 7655;		// beliebige Nummer zwischen -10.000 und 10.000
		int num2 = -4769;		// beliebige Nummer zwischen -10.000 und 10.000
		System.out.println("--- Vorher ---");
		System.out.println("Num1 = " + num1);
		System.out.println("Num2 = " + num2);
		
		/* {num1 = 7655 UND num2 = -4769} */
		num1 = num1 + num2;
		/* {num1 = num1 + num2 = 7655 + -4769 = 2886 UND num2 = -4769} */
		num2 = num1 - num2;
		/* {num1 = 2886 UND num2 = num1 - num2 = 2886 - -4769 = 7655} */
		num1 = num1 - num2;
		/* {num1 = num1 - num2 = 2886 - 7655 = -4769} */
		
		System.out.println("--- Nachher ---");
		System.out.println("Num1 = " + num1);
		System.out.println("Num2 = " + num2);
	}

}

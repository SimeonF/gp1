package h1;
public class H1_2_b {
	public static void main(String[] args) {
		int zahl1 = 1;
		int zahl2 = 10;
		int zahl3 = 100;
		int zahl4 = 1000;
		
		// Um zu zeigen, dass das folgende nicht nur f?r feste Zahlen gilt, wird ab jetzt mit variablen gerechnet.
		int help = zahl1 + zahl3;
		/* {zahl1=v1 UND zahl2=v2 UND zahl3=v3 UND zahl4=v4 UND help=v1+v3} */
		zahl1 = zahl2;
		/* {zahl1=v2 UND zahl2=v2 UND zahl3=v3 UND zahl4=v4 UND help=v1+v3} */
		zahl2 = help - zahl3;
		/* {zahl1=v2 UND zahl2=help-v3=v1 UND zahl3=v3 UND zahl4=v4 UND help=v1+v3} */
		zahl3 = zahl4;
		/* {zahl1=v2 UND zahl2=help-v3=v1 UND zahl3=v4 UND zahl4=v4 UND help=v1+v3} */
		zahl4 = help - zahl1;
		/* {zahl1=v2 UND zahl2=help-v3=v1 UND zahl3=v4 UND zahl4=help-v1=v3 UND help=v1+v3} */
	}
}

package h2;
public class H2_1 {
	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		int c = Integer.parseInt(args[2]);

		if (a <= b) {
			if (b < c) System.out.println("Kleinste: "+a+", Groesste: "+c);
			else if (c < a) System.out.println("Kleinste: "+c+", Groesste: "+b);
			else System.out.println("Kleinste: "+a+", Groesste: "+b);
		}
		else if (b < a) {
			if (a < c) System.out.println("Kleinste: "+b+", Groesste: "+c);
			else if (c < b) System.out.println("Kleinste: "+c+", Groesste: "+a);
			else System.out.println("Kleinste: "+b+", Groesste: "+a);
		}
	}
}

package h2;
public class H2_3_4 {
	public static void main(String[] args) {
		int exponent = Integer.parseInt(args[0]);
		int number = 2;
		while (exponent > 1) {
			number *= 2;
			exponent--;
		}

		for (int i = 1; i < number;i = i * 2) {
			String str = "";
			for (int cstar = i;cstar>0;cstar--) {
				for (int cspace = number / i;cspace>1;cspace--) str += " ";
				str += "*";
			}
			System.out.println(str);
		}
	}
}

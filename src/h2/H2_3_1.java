package h2;
public class H2_3_1 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		for (int x = 1; x <= n; x++) {
			for (int y = x; y != 0; y--) {
				System.out.print("x");
			}
			System.out.print("\n");
		}
		for (int x = n-1;x > 0; x--) {
			for (int y = x; y > 0; y--) {
				System.out.print("x");
			}
			System.out.print("\n");
		}
	}
}
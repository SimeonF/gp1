package h2;
public class H2_2 {
	public static void main(String[] args) {
		int rounds = Integer.parseInt(args[0]);	// Anzahl der Durchgaenge
		int gesamt = 0; 						// Gesamtanzahl der Zuege
		/* Feld:								   Keine 6, da sonst der rechte Rand fehlt
		 * 1	2	3	4	5
		 * 7	8	9	10	11
		 * 13	14	15	16	17
		 * 19	20	21	22	23
		 * 25	26	27	28	29 
		 */
		for (int j = rounds;j >0; j--) {
			int i;								// Zuege pro Durchgang
			int feld = 15;						// Startfeld, wird immer auf 15 gesetzt.
			for (i = 0;(feld%6 != 0 && feld > 0 && feld < 29); i++) {
				double dice = Math.random() * 5;
				if (dice < 1) feld--;			// Gehe nach Norden
				else if (dice < 2) feld++;		// Gehe nach Sueden
				else if (dice < 3) feld -= 6;	// Gehe nach Westen
				else if (dice < 4) feld += 6;	// Gehe nach Osten
			}									// Ansonsten stehen bleiben
			gesamt += i;
		}
		System.out.println("Gespielte Runden: "+rounds+" - Spielzuege insgesamt: "+gesamt+" - Durchschnitt: "+(double)gesamt/rounds);
	}
}

package h2;
public class H2_3_3 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		String str = "";					// Alles, was bisher angezeigt werden soll
		String str_end = "*";				// Anzahl der Sterne in der letzten Zeile
		for (int i = 1; i < n; i++) {
			str_end += "*";
			String temp = "";				// Anzahl der Sterne in der aktuellen Zeile
			for (int l = i;l > 0; l--) {
				temp += "*";
			}
			str += "\n" + temp + str;
		}
		System.out.println(str + "\n" + str_end);
	}
}

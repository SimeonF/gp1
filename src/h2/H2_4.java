package h2;
public class H2_4 {
	public static void main(String[] args) {
		int weeks = Integer.parseInt(args[0]);
		
		//
		// BEGIN: Kopfzeile
		System.out.print("\t\t");
		for (int week = 1; week <= weeks; week++) {
			System.out.print(week+"\t");
		}
		System.out.println("<- Wochen");
		// END: Kopfzeile
		//

		for (int trainee = 0; trainee < weeks; trainee++) {		// Beginne bei 0, damit wir zum Modulo 1 addieren koennen
			System.out.print("     "+(trainee+1)+"\t\t");		// Beschriftung Spalten
			for (int week=trainee;week<trainee+weeks;week++) {
				System.out.print(week%weeks+1+"\t");
			}
			System.out.print("\n");
		}
		System.out.println("Praktikanten");						// Beschriftung Spalten
	}

}

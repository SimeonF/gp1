package h2;
public class H2_3_2 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int countx = 1;
		int cspace = n-1;
		for (int i = n;i > 0; i--) {
			for (int h = cspace;h >0;h--) {
				System.out.print(" ");
			}
			for (int l = countx; l > 0; l--) {
				System.out.print("x");
			}
			System.out.print("\n");
			countx = countx + 2;
			cspace = cspace - 1;
		}
	}
}
package h6;

public class H6_3 {
	public static void main (String args[]) {
		MusterRekursiv(Integer.parseInt(args[0]));
	}

	public static void MusterRekursiv(int anzahl) {
		MusterRekursiv(anzahl, anzahl);
	}

	public static void MusterRekursiv(int zeichen, int spalten) {
		if (zeichen>0) {
			System.out.print("x");
			zeichen--;
			MusterRekursiv(zeichen,spalten);
		} else {
			System.out.println();
			spalten--;
			if (spalten>0) {
				MusterRekursiv(spalten, spalten);
			}
		}
	}
}
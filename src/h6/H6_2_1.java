package h6;

public class H6_2_1 {
	public static void main(String[] args) {
		System.out.print("Die Dezimalzahl "+args[0]+" ist im Bin??rcode ");
		printbinary(Integer.parseInt(args[0]));
	}
	
	static void printbinary(int dec) {
		if (dec < 0) {
			System.out.print("-");
			printbinary(dec*-1);
		}
		else if (dec <  2) System.out.print(dec);
		else if (dec >= 2) {
			printbinary(dec/2);
			System.out.print(dec%2);
		}
	}
}
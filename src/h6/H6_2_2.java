package h6;

public class H6_2_2 {
	public static void main(String[] args) {
		int num = Integer.parseInt(args[0]);		// Zahl
		int from = Integer.parseInt(args[1]);		// Von	System
		int to = Integer.parseInt(args[2]);			// Nach	System
		System.out.print("Die Zahl "+num+" im "+from+"er-System ist im "+to+"er-System: "+convert(num, from, to));
	}
	
	static int convert(int num, int from, int to) {
		if (num < 0) return convert(num*-1, from, to);
		if (from == to) return num;
		if (from == 10) return fromDecimal(num, to);
		if (to == 10) return toDecimal(num, from);
		else {
			num = toDecimal(num, from);
			return fromDecimal(num, to);
		}
	}


	static int toDecimal(int num, int from) {
		if (num < 10) return num;
		return from*toDecimal(num/10, from)+num%10;
	}
	
	static int fromDecimal(int num, int to) {
		return fromDecimal(num, to, "");
	}
	static int fromDecimal(int num, int to, String ret) {
		if (num < to) return Integer.parseInt(num + ret);
		else return fromDecimal((num-num%to)/to, to, num%to + ret);
	}
}
package h6;

public class H6_1 {
	public static void main(String[] args) {
		double ug = Double.parseDouble(args[0]);
		double og = Double.parseDouble(args[1]);

		System.out.println(nullstelle(ug,og));
	}

	static double nullstelle (double ug, double og) {
		double neuegrenze = (og+ug)/2;
		if ((og-ug) <= 0.00001) return neuegrenze;
		if (Math.sin(ug)*Math.sin(neuegrenze) <= 0) return nullstelle(ug,neuegrenze);
		return nullstelle(neuegrenze,og);
	}
}